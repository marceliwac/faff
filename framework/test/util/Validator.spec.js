const sinon = require('sinon');
const {it, describe, beforeEach} = require('mocha');
const {expect} = require('chai');

const Validator = require('../../src/util/Validator');

const validArrayOfFunctions = [() => {}, () => {}];
const validFunction = () => {}
const emptyArray = [];

describe('Validator', () => {
  describe('static functions', () => {
    describe('isFunction', () => {
      it('should return true when a function is supplied', () => {
        expect(Validator.isFunction(() => {})).to.be.true;
      });
      
      it('should return false when non-function is supplied', () => {
        expect(Validator.isFunction(validArrayOfFunctions)).to.be.false;
        expect(Validator.isFunction(emptyArray)).to.be.false;
        expect(Validator.isFunction(false)).to.be.false;
        expect(Validator.isFunction(true)).to.be.false;
        expect(Validator.isFunction('')).to.be.false;
        expect(Validator.isFunction('str')).to.be.false;
        expect(Validator.isFunction(0)).to.be.false;
        expect(Validator.isFunction(1)).to.be.false;
        expect(Validator.isFunction(null)).to.be.false;
        expect(Validator.isFunction(undefined)).to.be.false;
      });
    });

    describe('isArrayOfFunctions', () => {

      const sandbox = sinon.createSandbox();

      beforeEach(() => {
        sandbox.stub(Validator, 'isFunction');
      });

      afterEach(() => {
        sinon.restoreObject(Validator);
      });

      describe('when supplied argument is not an array', () => {
        it('should return false', () => {
          expect(Validator.isArrayOfFunctions(false)).to.be.false;
          expect(Validator.isArrayOfFunctions(true)).to.be.false;
          expect(Validator.isArrayOfFunctions('')).to.be.false;
          expect(Validator.isArrayOfFunctions('str')).to.be.false;
          expect(Validator.isArrayOfFunctions(0)).to.be.false;
          expect(Validator.isArrayOfFunctions(1)).to.be.false;
          expect(Validator.isArrayOfFunctions(null)).to.be.false;
          expect(Validator.isArrayOfFunctions(undefined)).to.be.false;
        });
      });

      describe('when supplied argument is an array', () => {
        describe('when the Validator.isFunction returns true for every element of the array', () => {
          beforeEach(() => {
            Validator.isFunction.returns(true);
          });

          it('should validate each element of an array using Validator.isFunction', () => {
            Validator.isArrayOfFunctions(validArrayOfFunctions);
            expect(Validator.isFunction.getCall(0).args[0]).to.equal(validArrayOfFunctions[0]);
            expect(Validator.isFunction.getCall(1).args[0]).to.equal(validArrayOfFunctions[1]);
            expect(Validator.isFunction.callCount).to.equal(2);
          });

          it('should return true', () => {
            expect(Validator.isArrayOfFunctions(validArrayOfFunctions)).to.be.true;
          });
        });
      });
    });

    describe('isNonEmptyArrayOfFunctions', () => {
      describe('when the Validator.isArrayOfFunctions called with supplied argument returns true', () => {
        const sandbox = sinon.createSandbox();

        beforeEach(() => {
          sandbox.stub(Validator, 'isArrayOfFunctions').returns(true);
        });

        afterEach(() => {
          sinon.restoreObject(Validator);
        });

        it('should validate the array of functions with Validator.isArrayOfFunctions', () => {
          const obj = {};
          Validator.isNonEmptyArrayOfFunctions(obj);
          expect(Validator.isArrayOfFunctions.getCall(0).args[0]).to.equal(obj);
        });

        it('should return false if the array is empty', () => {
          expect(Validator.isNonEmptyArrayOfFunctions([])).to.be.false;
        });
      });

      describe('when the Validator.isArrayOfFunctions called with supplied argument returns false', () => {
        const sandbox = sinon.createSandbox();

        beforeEach(() => {
          sandbox.stub(Validator, 'isArrayOfFunctions').returns(false);
        });

        afterEach(() => {
          sinon.restoreObject(Validator);
        });

        it('should return false', () => {
          const obj = {};
          expect(Validator.isNonEmptyArrayOfFunctions(obj)).to.be.false
        });
      });

      // Integration
      // describe('when supplied argument is not an array', () => {
      //   it('should return false', () => {
      //     expect(Validator.isNonEmptyArrayOfFunctions(false)).to.be.false;
      //     expect(Validator.isNonEmptyArrayOfFunctions(true)).to.be.false;
      //     expect(Validator.isNonEmptyArrayOfFunctions('')).to.be.false;
      //     expect(Validator.isNonEmptyArrayOfFunctions('str')).to.be.false;
      //     expect(Validator.isNonEmptyArrayOfFunctions(0)).to.be.false;
      //     expect(Validator.isNonEmptyArrayOfFunctions(1)).to.be.false;
      //     expect(Validator.isNonEmptyArrayOfFunctions(null)).to.be.false;
      //     expect(Validator.isNonEmptyArrayOfFunctions(undefined)).to.be.false;
      //   });
      // });
      // it('should return true if supplied argument is an array of functions', () => {
      //   expect(Validator.isNonEmptyArrayOfFunctions(validArrayOfFunctions)).to.be.true;
      // });
      //
      // it('should return false if supplied argument is an empty array', () => {
      //   expect(Validator.isNonEmptyArrayOfFunctions(emptyArray)).to.be.false;
      // });
      //
      // it('should return false if supplied argument is not an array of functions', () => {
      //   expect(Validator.isNonEmptyArrayOfFunctions(false)).to.be.false;
      //   expect(Validator.isNonEmptyArrayOfFunctions(true)).to.be.false;
      //   expect(Validator.isNonEmptyArrayOfFunctions('')).to.be.false;
      //   expect(Validator.isNonEmptyArrayOfFunctions('str')).to.be.false;
      //   expect(Validator.isNonEmptyArrayOfFunctions(0)).to.be.false;
      //   expect(Validator.isNonEmptyArrayOfFunctions(1)).to.be.false;
      //   expect(Validator.isNonEmptyArrayOfFunctions(null)).to.be.false;
      //   expect(Validator.isNonEmptyArrayOfFunctions(undefined)).to.be.false;
      // });
    });
    
    describe('isFunctionOrArrayOfFunctions', () => {
      describe('when the supplied argument is a function', () => {
        const sandbox = sinon.createSandbox();

        beforeEach(() => {
          sandbox.stub(Validator, 'isFunction').returns(true);
          sandbox.stub(Validator, 'isArrayOfFunctions').returns(true);
        });

        afterEach(() => {
          sinon.restoreObject(Validator);
        });

        it('should chek if argument is a function by calling Validator.isFunction with the supplied argument', () => {
          const obj = {};
          Validator.isFunctionOrArrayOfFunctions(obj)
          expect(Validator.isFunction.getCall(0).args[0]).to.equal(obj);
        })
      });

      describe('when the supplied argument is not a function', () => {
        const sandbox = sinon.createSandbox();

        beforeEach(() => {
          sandbox.stub(Validator, 'isFunction').returns(false);
          sandbox.stub(Validator, 'isArrayOfFunctions').returns(true);
        });

        afterEach(() => {
          sinon.restoreObject(Validator);
        });

        it('should chek if argument is an empty array or array of functions by calling Validator.isArrayOfFunctions with the supplied argument', () => {
          const obj = {};
          Validator.isFunctionOrArrayOfFunctions(obj)
          expect(Validator.isArrayOfFunctions.getCall(0).args[0]).to.equal(obj);
        })
      });
    });

    describe('isFunctionOrNonEmptyArrayOfFunctions', () => {
      describe('when the supplied argument is a function', () => {
        const sandbox = sinon.createSandbox();

        beforeEach(() => {
          sandbox.stub(Validator, 'isFunction').returns(true);
          sandbox.stub(Validator, 'isNonEmptyArrayOfFunctions').returns(true);
        });

        afterEach(() => {
          sinon.restoreObject(Validator);
        });

        it('should chek if argument is a function by calling Validator.isFunction with the supplied argument', () => {
          const obj = {};
          Validator.isFunctionOrNonEmptyArrayOfFunctions(obj)
          expect(Validator.isFunction.getCall(0).args[0]).to.equal(obj);
        })
      });

      describe('when the supplied argument is not a function', () => {
        const sandbox = sinon.createSandbox();

        beforeEach(() => {
          sandbox.stub(Validator, 'isFunction').returns(false);
          sandbox.stub(Validator, 'isNonEmptyArrayOfFunctions').returns(true);
        });

        afterEach(() => {
          sinon.restoreObject(Validator);
        });

        it('should chek if argument is an empty array or array of functions by calling Validator.isNonEmptyArrayOfFunctions with the supplied argument', () => {
          const obj = {};
          Validator.isFunctionOrNonEmptyArrayOfFunctions(obj)
          expect(Validator.isNonEmptyArrayOfFunctions.getCall(0).args[0]).to.equal(obj);
        })
      });
    });

    describe('validateConfig', () => {
      const validConfig = {
        onRequest: () => {},
        onResponse: () => {},
        onHttpResponseReturned: () => {},
        onNonHttpResponseReturned: () => {},
        onHttpResponseThrown: () => {},
        onNonHttpResponseThrown: () => {},
        handleError: () => {},
        handleResponse: () => {},
        handleNoResponse: () => {},
      }

      const partialConfig = {
        onRequest: () => {},
        onNonHttpResponseThrown: () => {},
        handleError: () => {},
      };

      const sandbox = sinon.createSandbox();

      beforeEach(() => {
        sandbox.stub(Validator, 'isFunctionOrArrayOfFunctions').returns(true);
        sandbox.stub(Validator, 'isFunction').returns(true);
      });

      afterEach(() => {
        sinon.restoreObject(Validator);
      });

      describe('when called without allFieldsRequired defined', () => {
        it('should treat it as if allFieldsRequired was true and attempt to validate', () => {
          Validator.validateConfig({});
          const validationCalled = Validator.isFunction.called || Validator.isFunctionOrArrayOfFunctions.called;
          expect(validationCalled).to.be.true;
        });
      });

      describe('when called with allFieldsRequired = true', () => {
        it('should attempt to validate when empty object is passed', () => {
          Validator.validateConfig({}, true);
          const validatorCalled =
            Validator.isFunctionOrArrayOfFunctions.called
            || Validator.isFunction.called
          expect(validatorCalled).to.be.true;
        });

        it('should validate all properties', () => {
          /*
           6 methods validated using isFunctionOrArrayOfFunctions:
           [onRequest, onResponse, onHttpResponseReturned, onNonHttpResponseReturned,
           onHttpResponseThrown, onNonHttpResponseThrown]

           3 methods validated using isFunction:
           [handleError, handleResponse, handleNoResponse]
           */
          expect(() => Validator.validateConfig(validConfig, true)).to.not.throw();
          expect(Validator.isFunctionOrArrayOfFunctions.callCount).to.equal(6);
          expect(Validator.isFunctionOrArrayOfFunctions.getCall(0).args[0]).to.equal(validConfig.onRequest);
          expect(Validator.isFunctionOrArrayOfFunctions.getCall(1).args[0]).to.equal(validConfig.onResponse);
          expect(Validator.isFunctionOrArrayOfFunctions.getCall(2).args[0]).to.equal(validConfig.onHttpResponseReturned);
          expect(Validator.isFunctionOrArrayOfFunctions.getCall(3).args[0]).to.equal(validConfig.onNonHttpResponseReturned);
          expect(Validator.isFunctionOrArrayOfFunctions.getCall(4).args[0]).to.equal(validConfig.onHttpResponseThrown);
          expect(Validator.isFunctionOrArrayOfFunctions.getCall(5).args[0]).to.equal(validConfig.onNonHttpResponseThrown);
          expect(Validator.isFunction.callCount).to.equal(3);
          expect(Validator.isFunction.getCall(0).args[0]).to.equal(validConfig.handleError);
          expect(Validator.isFunction.getCall(1).args[0]).to.equal(validConfig.handleResponse);
          expect(Validator.isFunction.getCall(2).args[0]).to.equal(validConfig.handleNoResponse);
        });

        it('should validate onRequest property by calling Validator.isFunctionOrArrayOfFunctions', () => {
          Validator.validateConfig(validConfig, true);
          expect(Validator.isFunctionOrArrayOfFunctions.calledWith(validConfig.onRequest)).to.be.true;
        });

        it('should validate onResponse property by calling Validator.isFunctionOrArrayOfFunctions', () => {
          Validator.validateConfig(validConfig, true);
          expect(Validator.isFunctionOrArrayOfFunctions.calledWith(validConfig.onResponse)).to.be.true;
        });

        it('should validate onHttpResponseReturned property by calling Validator.isFunctionOrArrayOfFunctions', () => {
          Validator.validateConfig(validConfig, true);
          expect(Validator.isFunctionOrArrayOfFunctions.calledWith(validConfig.onHttpResponseReturned)).to.be.true;
        });

        it('should validate onNonHttpResponseReturned property by calling Validator.isFunctionOrArrayOfFunctions', () => {
          Validator.validateConfig(validConfig, true);
          expect(Validator.isFunctionOrArrayOfFunctions.calledWith(validConfig.onNonHttpResponseReturned)).to.be.true;
        });

        it('should validate onHttpResponseThrown property by calling Validator.isFunctionOrArrayOfFunctions', () => {
          Validator.validateConfig(validConfig, true);
          expect(Validator.isFunctionOrArrayOfFunctions.calledWith(validConfig.onHttpResponseThrown)).to.be.true;
        });

        it('should validate onNonHttpResponseThrown property by calling Validator.isFunctionOrArrayOfFunctions', () => {
          Validator.validateConfig(validConfig, true);
          expect(Validator.isFunctionOrArrayOfFunctions.calledWith(validConfig.onNonHttpResponseThrown)).to.be.true;
        });

        it('should validate handleError property by calling Validator.isFunction', () => {
          Validator.validateConfig(validConfig, true);
          expect(Validator.isFunction.calledWith(validConfig.handleError)).to.be.true;
        });

        it('should validate handleResponse property by calling Validator.isFunction', () => {
          Validator.validateConfig(validConfig, true);
          expect(Validator.isFunction.calledWith(validConfig.handleResponse)).to.be.true;
        });

        it('should validate handleNoResponse property by calling Validator.isFunction', () => {
          Validator.validateConfig(validConfig, true);
          expect(Validator.isFunction.calledWith(validConfig.handleNoResponse)).to.be.true;
        });
      });

      describe('when called with allFieldsRequired = false', () => {
        it('should not validate anything when called with empty object', () => {
          expect(() => Validator.validateConfig({}, false)).to.not.throw();
          const validatorCalled =
            Validator.isFunctionOrArrayOfFunctions.called
            || Validator.isFunction.called
          expect(validatorCalled).to.be.false;
        });

        it('should only validate defined properties', () => {
          expect(() => Validator.validateConfig(partialConfig, false)).to.not.throw();
          expect(Validator.isFunctionOrArrayOfFunctions.getCall(0).args[0]).to.equal(partialConfig.onRequest);
          expect(Validator.isFunctionOrArrayOfFunctions.getCall(1).args[0]).to.equal(partialConfig.onNonHttpResponseThrown);
          expect(Validator.isFunctionOrArrayOfFunctions.callCount).to.equal(2);
          expect(Validator.isFunction.getCall(0).args[0]).to.equal(partialConfig.handleError);
          expect(Validator.isFunction.callCount).to.equal(1);
        });

        it('should validate all defined properties', () => {
          expect(() => Validator.validateConfig(validConfig, false)).to.not.throw();
          /*
           6 methods validated using isFunctionOrArrayOfFunctions:
           [onRequest, onResponse, onHttpResponseReturned, onNonHttpResponseReturned,
           onHttpResponseThrown, onNonHttpResponseThrown]

           3 methods validated using isFunction:
           [handleError, handleResponse, handleNoResponse]
           */
          expect(Validator.isFunctionOrArrayOfFunctions.callCount).to.equal(6);
          expect(Validator.isFunctionOrArrayOfFunctions.getCall(0).args[0]).to.equal(validConfig.onRequest);
          expect(Validator.isFunctionOrArrayOfFunctions.getCall(1).args[0]).to.equal(validConfig.onResponse);
          expect(Validator.isFunctionOrArrayOfFunctions.getCall(2).args[0]).to.equal(validConfig.onHttpResponseReturned);
          expect(Validator.isFunctionOrArrayOfFunctions.getCall(3).args[0]).to.equal(validConfig.onNonHttpResponseReturned);
          expect(Validator.isFunctionOrArrayOfFunctions.getCall(4).args[0]).to.equal(validConfig.onHttpResponseThrown);
          expect(Validator.isFunctionOrArrayOfFunctions.getCall(5).args[0]).to.equal(validConfig.onNonHttpResponseThrown);
          expect(Validator.isFunction.callCount).to.equal(3);
          expect(Validator.isFunction.getCall(0).args[0]).to.equal(validConfig.handleError);
          expect(Validator.isFunction.getCall(1).args[0]).to.equal(validConfig.handleResponse);
          expect(Validator.isFunction.getCall(2).args[0]).to.equal(validConfig.handleNoResponse);
        });

        it('should validate onRequest property by calling Validator.isFunctionOrArrayOfFunctions', () => {
          Validator.validateConfig(validConfig, true);
          expect(Validator.isFunctionOrArrayOfFunctions.calledWith(validConfig.onRequest)).to.be.true;
        });

        it('should validate onResponse property by calling Validator.isFunctionOrArrayOfFunctions', () => {
          Validator.validateConfig(validConfig, true);
          expect(Validator.isFunctionOrArrayOfFunctions.calledWith(validConfig.onResponse)).to.be.true;
        });

        it('should validate onHttpResponseReturned property by calling Validator.isFunctionOrArrayOfFunctions', () => {
          Validator.validateConfig(validConfig, true);
          expect(Validator.isFunctionOrArrayOfFunctions.calledWith(validConfig.onHttpResponseReturned)).to.be.true;
        });

        it('should validate onNonHttpResponseReturned property by calling Validator.isFunctionOrArrayOfFunctions', () => {
          Validator.validateConfig(validConfig, true);
          expect(Validator.isFunctionOrArrayOfFunctions.calledWith(validConfig.onNonHttpResponseReturned)).to.be.true;
        });

        it('should validate onHttpResponseThrown property by calling Validator.isFunctionOrArrayOfFunctions', () => {
          Validator.validateConfig(validConfig, true);
          expect(Validator.isFunctionOrArrayOfFunctions.calledWith(validConfig.onHttpResponseThrown)).to.be.true;
        });

        it('should validate onNonHttpResponseThrown property by calling Validator.isFunctionOrArrayOfFunctions', () => {
          Validator.validateConfig(validConfig, true);
          expect(Validator.isFunctionOrArrayOfFunctions.calledWith(validConfig.onNonHttpResponseThrown)).to.be.true;
        });

        it('should validate handleError property by calling Validator.isFunction', () => {
          Validator.validateConfig(validConfig, true);
          expect(Validator.isFunction.calledWith(validConfig.handleError)).to.be.true;
        });

        it('should validate handleResponse property by calling Validator.isFunction', () => {
          Validator.validateConfig(validConfig, true);
          expect(Validator.isFunction.calledWith(validConfig.handleResponse)).to.be.true;
        });

        it('should validate handleNoResponse property by calling Validator.isFunction', () => {
          Validator.validateConfig(validConfig, true);
          expect(Validator.isFunction.calledWith(validConfig.handleNoResponse)).to.be.true;
        });
      });

      describe('when isFunctionOrArrayOfFunctions returns false', () => {
        beforeEach(() => {
          Validator.isFunctionOrArrayOfFunctions.returns(false);
        });

        it('should throw a TypeError', () => {
          expect(() => Validator.validateConfig(validConfig)).to.throw(TypeError);
          expect(Validator.isFunctionOrArrayOfFunctions.called).to.be.true;
        });
      });

      describe('when isFunction returns false', () => {
        beforeEach(() => {
          Validator.isFunction.returns(false);
        });

        it('should throw a TypeError', () => {
          expect(() => Validator.validateConfig(validConfig)).to.throw(TypeError);
          expect(Validator.isFunction.called).to.be.true;
        });
      });
    });
  });
});

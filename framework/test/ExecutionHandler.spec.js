const {it, describe, beforeEach} = require('mocha');
const chai = require('chai');
const chaiAsPromised = require('chai-as-promised');
chai.use(chaiAsPromised);
let expect = chai.expect;
const sinon = require('sinon');

const ExecutionHandler = require('../src/ExecutionHandler');
const HttpResponse = require('../src/HttpResponse');

const Validator = require('../src/util/Validator');

const validConfig = {
  onRequest: () => {},
  onResponse: () => {},
  onHttpResponseReturned: () => {},
  onNonHttpResponseReturned: () => {},
  onHttpResponseThrown: () => {},
  onNonHttpResponseThrown: () => {},
  handleError: () => {},
  handleResponse: () => {},
  handleNoResponse: () => {}
}

const validConfigArrays = {
  onRequest: [() => {}],
  onResponse: [() => {}],
  onHttpResponseReturned: [() => {}],
  onNonHttpResponseReturned: [() => {}],
  onHttpResponseThrown: [() => {}],
  onNonHttpResponseThrown: [() => {}],
  handleError: () => {},
  handleResponse: () => {},
  handleNoResponse: () => {}
}

const partialConfig = {
  onRequest: () => {},
  onNonHttpResponseThrown: () => {},
  handleError: () => {},
};

const validArrayOfFunctions = [() => {}, () => {}];

describe('ExecutionHandler', () => {
  describe('static properties', () => {
    describe('DEFAULT_CONFIG', () => {
      it('should be defined as an object', () => {
        expect(ExecutionHandler.DEFAULT_CONFIG).to.be.an('object');
      });

      it('should be valid as validated by Validator.validateConfig', () => {
        expect(() => Validator.validateConfig(ExecutionHandler.DEFAULT_CONFIG)).to.not.throw();
      });
    })
  })

  describe('static methods', () => {
    describe('assembleConfig', () => {
      it('should return default config if no config was provided', () => {
        expect(ExecutionHandler.assembleConfig()).to.be.equal(ExecutionHandler.DEFAULT_CONFIG);
      });

      it('should return default config if provided config was an empty object', () => {
        expect(ExecutionHandler.assembleConfig({})).to.be.equal(ExecutionHandler.DEFAULT_CONFIG);
      });

      it('should return throw a TypeError if provided config was not an object or undefined', () => {
        expect(() => ExecutionHandler.assembleConfig('string')).to.be.throw(TypeError);
        expect(() => ExecutionHandler.assembleConfig('')).to.be.throw(TypeError);
        expect(() => ExecutionHandler.assembleConfig(1)).to.be.throw(TypeError);
        expect(() => ExecutionHandler.assembleConfig(0)).to.be.throw(TypeError);
        expect(() => ExecutionHandler.assembleConfig(true)).to.be.throw(TypeError);
        expect(() => ExecutionHandler.assembleConfig(false)).to.be.throw(TypeError);
      });

      it('should return an object that extends default configuration when partial config is passed', () => {
        const assembledConfig = ExecutionHandler.assembleConfig(partialConfig);
        expect(assembledConfig.onRequest).to.equal(partialConfig.onRequest);
        expect(assembledConfig.onResponse).to.equal(ExecutionHandler.DEFAULT_CONFIG.onResponse);
        expect(assembledConfig.onHttpResponseReturned).to.equal(ExecutionHandler.DEFAULT_CONFIG.onHttpResponseReturned);
        expect(assembledConfig.onNonHttpResponseReturned).to.equal(ExecutionHandler.DEFAULT_CONFIG.onNonHttpResponseReturned);
        expect(assembledConfig.onHttpResponseThrown).to.equal(ExecutionHandler.DEFAULT_CONFIG.onHttpResponseThrown);
        expect(assembledConfig.onNonHttpResponseThrown).to.equal(partialConfig.onNonHttpResponseThrown);
        expect(assembledConfig.handleError).to.equal(partialConfig.handleError);
        expect(assembledConfig.handleResponse).to.equal(ExecutionHandler.DEFAULT_CONFIG.handleResponse);
        expect(assembledConfig.handleNoResponse).to.equal(ExecutionHandler.DEFAULT_CONFIG.handleNoResponse);
      });

    });

    describe('processChainOfFunctions', () => {

      describe('regardless of validation results', () => {
        const sandbox = sinon.createSandbox();

        beforeEach(() => {
          sandbox.stub(Validator, 'isArrayOfFunctions');
        });

        afterEach(() => {
          sinon.restoreObject(Validator);
        });

        it('should call the Validator.isArrayOfFunctions with it supplied arrayOfFunctions parameter', () => {
          expect(ExecutionHandler.processChainOfFunctions(validArrayOfFunctions)).to.eventually.be.fulfilled;
          expect(Validator.isArrayOfFunctions.getCall(0).args[0]).to.equal(validArrayOfFunctions);
        });
      });

      describe('when validations fails', () => {
        const sandbox = sinon.createSandbox();

        beforeEach(() => {
          sandbox.stub(Validator, 'isArrayOfFunctions').returns(false);
        });

        afterEach(() => {
          sinon.restoreObject(Validator);
        });

        it('should eventually reject with a TypeError', () => {
          expect(ExecutionHandler.processChainOfFunctions({})).to.eventually.be.rejectedWith(TypeError);
        });

        it('should not execute supplied functions', async () => {
          const spy = sinon.spy();
          await ExecutionHandler.processChainOfFunctions([spy]).catch(() => {});
          expect(spy.callCount).to.equal(0);
        });
      });

      describe('when validation succeeds', () => {
        const returnValue0 = {a: 'a'};
        const returnValue1 = {b: 'b'};
        const sandbox = sinon.createSandbox();
        const syncStub0 = sandbox.stub();
        const syncStub1 = sandbox.stub();
        const syncStub2 = sandbox.stub();
        const syncStubReturning0 = sandbox.stub().returns(returnValue0)
        const syncStubReturning1 = sandbox.stub().returns(returnValue1)
        const asyncStub0 = sandbox.stub().resolves();
        const asyncStub1 = sandbox.stub().resolves();
        const asyncStub2 = sandbox.stub().resolves();
        const asyncStubReturning0 = sandbox.stub().resolves(returnValue0)
        const asyncStubReturning1 = sandbox.stub().resolves(returnValue1)
        const payloadWithResponse = {response: {}};
        const payloadWithoutResponse = {};

        before(() => {
          sandbox.stub(Validator, 'isArrayOfFunctions').returns(true);
        });

        beforeEach(() => {
          sandbox.resetHistory();
        })

        after(() => {
          sinon.restoreObject(Validator);
        });

        describe('when validating functions', () => {
          it('should validate the arrayOfFunctions parameter using Validator.isArrayOfFunctions', async () => {
            const obj = {};
            await expect(ExecutionHandler.processChainOfFunctions(obj, {})).to.eventually.be.fulfilled;
            expect(Validator.isArrayOfFunctions.getCall(0).args[0]).to.equal(obj)
          });
        });

        describe('when supplied functions are synchronous', () => {
          it('should execute the supplied functions in order they were supplied in', async () => {
            await ExecutionHandler.processChainOfFunctions([syncStub0, syncStub1, syncStub2], {}).then(() => {
              expect(syncStub0.calledImmediatelyBefore(syncStub1)).to.be.true;
              expect(syncStub1.calledImmediatelyBefore(syncStub2)).to.be.true;
              expect(syncStub0.callCount).to.equal(1);
              expect(syncStub1.callCount).to.equal(1);
              expect(syncStub2.callCount).to.equal(1);
            });
          });

          it('should return the value of first function that returns non-undefined result', async () => {
            await expect(
              ExecutionHandler.processChainOfFunctions([syncStub0, syncStubReturning0, syncStubReturning1], {})
            ).to.eventually.equal(returnValue0);
          });

          it('should not continue execution after non-undefined return value was returned by one of the functions', async () => {
            await expect(
              ExecutionHandler.processChainOfFunctions([syncStub0, syncStubReturning0, syncStub1], {})
            ).to.eventually.equal(returnValue0);
            expect(syncStub1.callCount).to.equal(0)
          });

          it('should pass payload to each of the supplied functions', async () => {
            await expect(
              ExecutionHandler.processChainOfFunctions([syncStub0, syncStub1], payloadWithoutResponse)
            ).to.eventually.be.fulfilled;
            expect(syncStub0.getCall(0).args[0]).to.equal(payloadWithoutResponse);
            expect(syncStub1.getCall(0).args[0]).to.equal(payloadWithoutResponse);
          });
        });

        describe('when supplied functions are asynchronous', () => {
          it('should execute the supplied functions in order they were supplied in', async () => {
            await ExecutionHandler.processChainOfFunctions([asyncStub0, asyncStub1, asyncStub2], {}).then(() => {
              expect(asyncStub0.calledImmediatelyBefore(asyncStub1)).to.be.true;
              expect(asyncStub1.calledImmediatelyBefore(asyncStub2)).to.be.true;
              expect(asyncStub0.callCount).to.equal(1);
              expect(asyncStub1.callCount).to.equal(1);
              expect(asyncStub2.callCount).to.equal(1);
            });
          });

          it('should return the value of first function that resolves to non-undefined result', async () => {
            await expect(
              ExecutionHandler.processChainOfFunctions([asyncStub0, asyncStubReturning0, asyncStubReturning1], {})
            ).to.eventually.equal(returnValue0);
          });

          it('should not continue execution after non-undefined return value was resolved to by one of the functions', async () => {
            await expect(
              ExecutionHandler.processChainOfFunctions([asyncStub0, asyncStubReturning0, asyncStub1], {})
            ).to.eventually.equal(returnValue0);
            expect(asyncStub1.callCount).to.equal(0)
          });

          it('should pass payload to each of the supplied functions', async () => {
            await expect(
              ExecutionHandler.processChainOfFunctions([asyncStub0, asyncStub1], payloadWithoutResponse)
            ).to.eventually.be.fulfilled;
            expect(asyncStub0.getCall(0).args[0]).to.equal(payloadWithoutResponse);
            expect(asyncStub1.getCall(0).args[0]).to.equal(payloadWithoutResponse);
          });
        });

        describe('when payload has shared parameter defined', () => {
          describe('when one of the functions modifies shared parameter', () => {
            function setSharedValueTo1 ({shared}) {
              shared.value = 1;
            }

            it('should provide the same shared object to the following function', async () => {
              const shared = {};
              await expect(
                ExecutionHandler.processChainOfFunctions([setSharedValueTo1, syncStub0], {shared})
              ).to.eventually.be.fulfilled;
              expect(syncStub0.getCall(0).args[0].shared).to.equal(shared);
            });

            it('should provide the same shared object to the following function', async () => {
              const shared = {};
              await expect(
                ExecutionHandler.processChainOfFunctions([setSharedValueTo1, syncStub0], {shared})
              ).to.eventually.be.fulfilled;
              expect(shared.value).to.equal(1);
            });
          });
        });

        describe('when payload has response parameter defined', () => {
          describe('when one of the functions returns a value', () => {
            it('should return the value returned by the function', async () => {
              await expect(
                ExecutionHandler.processChainOfFunctions([syncStub0, syncStubReturning0], payloadWithResponse)
              ).to.eventually.equal(returnValue0);
            });
          });

          describe('when none of the functions return a value', () => {
            it('should return the response from payload', async () => {
              await expect(
                ExecutionHandler.processChainOfFunctions([syncStub0, syncStub1], payloadWithResponse)
              ).to.eventually.equal(payloadWithResponse.response);
            });
          });
        });

        describe('when no functions are supplied', () => {
          describe('when payload.response parameter is not undefined', () => {
            it('should return the response provided in the response parameter', async () => {
              const obj = {};
              await expect(ExecutionHandler.processChainOfFunctions([], {response: obj})).to.eventually.equal(obj);
            });
          });

          describe('when payload.response parameter is undefined', () => {
            it('should return undefined', async () => {
              await expect(ExecutionHandler.processChainOfFunctions([], {response: undefined})).to.eventually.equal(undefined);
            });
          });
        });
      });
    });
  });

  describe('constructor', () => {
    let validatorValidateConfigStaticStub;
    let executionHandlerAssembleConfigStaticStub;
    let executionHandlerInitialiseInstanceStub;
    const assembledConfig = {};

    beforeEach(() => {
      validatorValidateConfigStaticStub = sinon.stub(require('../src/util/Validator'), 'validateConfig').returns(true);
      executionHandlerAssembleConfigStaticStub = sinon.stub(ExecutionHandler, 'assembleConfig').returns(assembledConfig);
      executionHandlerInitialiseInstanceStub = sinon.stub(ExecutionHandler.prototype, 'initialise');
    });

    afterEach(() => {
      validatorValidateConfigStaticStub.restore();
      executionHandlerAssembleConfigStaticStub.restore();
      executionHandlerInitialiseInstanceStub.restore();
    });

    describe('when called with no config', () => {
      let executionHandler;

      beforeEach(() => {
        executionHandler = new ExecutionHandler();
      });

      it('should call Validator.validateConfig only once', () => {
        expect(validatorValidateConfigStaticStub.callCount).to.equal(1);
      });

      it('should call the ExecutionHandler.assembleConfig', () => {
        expect(ExecutionHandler.assembleConfig.callCount).to.equal(1);
      });

      it('should call Validator.validateConfig with a default config', () => {
        expect(validatorValidateConfigStaticStub.getCall(0).args[0]).to.equal(assembledConfig);
      });

      it('should call the initialise function with the assembled config', () => {
        expect(executionHandlerInitialiseInstanceStub.callCount).to.equal(1);
        expect(executionHandlerInitialiseInstanceStub.getCall(0).args[0]).to.equal(assembledConfig);
      });
    });

    describe('when called with valid (partial or full) config', () => {
      let executionHandler;

      beforeEach(() => {
        executionHandler = new ExecutionHandler(partialConfig);
      });

      it('should call Validator.validateConfig twice', () => {
        expect(validatorValidateConfigStaticStub.callCount).to.equal(2);
      });

      it('should call Validator.validateConfig with partial config and allFieldsRequired set as false first time', () => {
        expect(validatorValidateConfigStaticStub.getCall(0).args[0]).to.equal(partialConfig);
        expect(validatorValidateConfigStaticStub.getCall(0).args[1]).to.be.false;
      });

      it('should call the ExecutionHandler.assembleConfig', () => {
        expect(ExecutionHandler.assembleConfig.callCount).to.equal(1);
      });

      it('should call Validator.validateConfig with assembled config and allFieldsRequired set as true second time', () => {
        expect(validatorValidateConfigStaticStub.getCall(1).args[0]).to.equal(assembledConfig);
        expect(validatorValidateConfigStaticStub.getCall(1).args[1]).to.be.oneOf([undefined, true]);
      });

      it('should call initialise instance function with an assembled config', () => {
        expect(executionHandlerInitialiseInstanceStub.getCall(0).args[0]).to.equal(assembledConfig);
      });
    });
  });

  describe('instance methods', () => {
    // TODO: This can be tested by defining the system under test (sut) explicitly and importing
    //  the method that will be tested into it. For example - defining an object that contains
    //  method under test (mut), and stubbing all dependencies of that method (including the
    //  other instance methods as well as 'this' object etc. The imported modules should also be
    //  mocked, but that can be done separately (i.e. by stubbing the module itself ?).
    describe('initialise', () => {
      let sut;

      beforeEach(() => {
        sut = {
          mut: ExecutionHandler.prototype.initialise,
        }
      });

      describe('', () => {

      });




      // beforeEach(() => {
      //
      // });
      //
      // const validConfigArrays = {
      //   onRequest: [() => {}],
      //   onResponse: [() => {}],
      //   onHttpResponseReturned: [() => {}],
      //   onNonHttpResponseReturned: [() => {}],
      //   onHttpResponseThrown: [() => {}],
      //   onNonHttpResponseThrown: [() => {}],
      //   handleError: () => {},
      //   handleResponse: () => {},
      //   handleNoResponse:() => {}
      // };
      //
      // describe('when the assembled config contains functions', () => {
      //   const sandbox = sinon.createSandbox();
      //   let executionHandler;
      //
      //   beforeEach(() => {
      //     sandbox.stub(ExecutionHandler, 'assembleConfig').returns(validConfig);
      //     executionHandler = new ExecutionHandler(validConfig);
      //   });
      //
      //   afterEach(() => {
      //     sinon.restoreObject(ExecutionHandler);
      //   });
      //
      //   it('should set the onRequest value to an array storing the function provided in the config', () => {
      //     expect(executionHandler.onRequest).to.not.be.undefined;
      //     expect(executionHandler.onRequest[0]).to.equal(validConfig.onRequest);
      //   });
      //
      //   it('should set the onResponse value to an array storing the function provided in the config', () => {
      //     expect(executionHandler.onResponse).to.not.be.undefined;
      //     expect(executionHandler.onResponse[0]).to.equal(validConfig.onResponse);
      //   });
      //
      //   it('should set the onHttpResponseReturned value to an array storing the function provided in the config', () => {
      //     expect(executionHandler.onHttpResponseReturned).to.not.be.undefined;
      //     expect(executionHandler.onHttpResponseReturned[0]).to.equal(validConfig.onHttpResponseReturned);
      //   });
      //
      //   it('should set the onNonHttpResponseReturned value to an array storing the function provided in the config', () => {
      //     expect(executionHandler.onNonHttpResponseReturned).to.not.be.undefined;
      //     expect(executionHandler.onNonHttpResponseReturned[0]).to.equal(validConfig.onNonHttpResponseReturned);
      //   });
      //
      //   it('should set the onHttpResponseThrown value to an array storing the function provided in the config', () => {
      //     expect(executionHandler.onHttpResponseThrown).to.not.be.undefined;
      //     expect(executionHandler.onHttpResponseThrown[0]).to.equal(validConfig.onHttpResponseThrown);
      //   });
      //
      //   it('should set the onNonHttpResponseThrown value to an array storing the function provided in the config', () => {
      //     expect(executionHandler.onNonHttpResponseThrown).to.not.be.undefined;
      //     expect(executionHandler.onNonHttpResponseThrown[0]).to.equal(validConfig.onNonHttpResponseThrown);
      //   });
      //
      //   it('should set the handleError value to the value provided in the config', () => {
      //     expect(executionHandler.handleError).to.not.be.undefined;
      //     expect(executionHandler.handleError).to.equal(validConfig.handleError);
      //   });
      //
      //   it('should set the handleResponse value to the value provided in the config', () => {
      //     expect(executionHandler.handleResponse).to.not.be.undefined;
      //     expect(executionHandler.handleResponse).to.equal(validConfig.handleResponse);
      //   });
      //
      //   it('should set the handleNoResponse value to the value provided in the config', () => {
      //     expect(executionHandler.handleNoResponse).to.not.be.undefined;
      //     expect(executionHandler.handleNoResponse).to.equal(validConfig.handleNoResponse);
      //   });
      // });
      //
      // describe('when the assembled config contains arrays of functions', () => {
      //     const sandbox = sinon.createSandbox();
      //     let executionHandler;
      //
      //     beforeEach(() => {
      //       sandbox.stub(ExecutionHandler, 'assembleConfig').returns(validConfigArrays);
      //       executionHandler = new ExecutionHandler(validConfig);
      //     });
      //
      //     afterEach(() => {
      //       sinon.restoreObject(ExecutionHandler);
      //     });
      //
      //     it('should set the onRequest value to the one provided in the config', () => {
      //       expect(executionHandler.onRequest).to.not.be.undefined;
      //       expect(executionHandler.onRequest[0]).to.equal(validConfigArrays.onRequest[0]);
      //     });
      //
      //     it('should set the onResponse value to the value provided in the config', () => {
      //       expect(executionHandler.onResponse).to.not.be.undefined;
      //       expect(executionHandler.onResponse[0]).to.equal(validConfigArrays.onResponse[0]);
      //     });
      //
      //     it('should set the onHttpResponseReturned value to the value provided in the config', () => {
      //       expect(executionHandler.onHttpResponseReturned).to.not.be.undefined;
      //       expect(executionHandler.onHttpResponseReturned[0]).to.equal(validConfigArrays.onHttpResponseReturned[0]);
      //     });
      //
      //     it('should set the onNonHttpResponseReturned value to the value provided in the config', () => {
      //       expect(executionHandler.onNonHttpResponseReturned).to.not.be.undefined;
      //       expect(executionHandler.onNonHttpResponseReturned[0]).to.equal(validConfigArrays.onNonHttpResponseReturned[0]);
      //     });
      //
      //     it('should set the onHttpResponseThrown value to the value provided in the config', () => {
      //       expect(executionHandler.onHttpResponseThrown).to.not.be.undefined;
      //       expect(executionHandler.onHttpResponseThrown[0]).to.equal(validConfigArrays.onHttpResponseThrown[0]);
      //     });
      //
      //     it('should set the onNonHttpResponseThrown value to the value provided in the config', () => {
      //       expect(executionHandler.onNonHttpResponseThrown).to.not.be.undefined;
      //       expect(executionHandler.onNonHttpResponseThrown[0]).to.equal(validConfigArrays.onNonHttpResponseThrown[0]);
      //     });
      //
      //     it('should set the handleError value to the value provided in the config', () => {
      //       expect(executionHandler.handleError).to.not.be.undefined;
      //       expect(executionHandler.handleError[0]).to.equal(validConfig.handleError[0]);
      //     });
      //
      //     it('should set the handleResponse value to the value provided in the config', () => {
      //       expect(executionHandler.handleResponse).to.not.be.undefined;
      //       expect(executionHandler.handleResponse[0]).to.equal(validConfig.handleResponse[0]);
      //     });
      //
      //     it('should set the handleNoResponse value to the value provided in the config', () => {
      //       expect(executionHandler.handleNoResponse).to.not.be.undefined;
      //       expect(executionHandler.handleNoResponse[0]).to.equal(validConfig.handleNoResponse[0]);
      //     });
      //   });
    });

    describe('postResponseResolved', () => {
      const sandbox = sinon.createSandbox();
      const callback = () => {};
      const obj = {};
      let executionHandler;

      beforeEach(() => {
        executionHandler = new ExecutionHandler(validConfig);
        sandbox.stub(executionHandler, 'handleResponse');
        sandbox.stub(executionHandler, 'handleNoResponse');
      });

      afterEach(() => {
        sandbox.resetHistory();
      });

      it('should call the handleResponse instance method with handlerResponse and callback if handlerResponse is defined', () => {
        executionHandler.postResponseResolved(obj, callback);
        expect(executionHandler.handleResponse.callCount).to.equal(1);
        expect(executionHandler.handleResponse.getCall(0).args[0]).to.equal(obj);
        expect(executionHandler.handleResponse.getCall(0).args[1]).to.equal(callback);
      });

      it('should call the handleNoResponse instance method with callback if handlerResponse is undefined', () => {
        executionHandler.postResponseResolved(undefined, callback);
        expect(executionHandler.handleNoResponse.callCount).to.equal(1);
        expect(executionHandler.handleNoResponse.getCall(0).args[0]).to.equal(callback);
      });
    });

    describe('postResponseRejected', () => {
      const sandbox = sinon.createSandbox();
      const callback = () => {};
      const error = {};
      let executionHandler;

      beforeEach(() => {
        executionHandler = new ExecutionHandler(validConfig);
        sandbox.stub(executionHandler, 'handleError');
      });

      afterEach(() => {
        sandbox.resetHistory();
      });

      it('should call the handleError instance method with handlerError and callback', () => {
        executionHandler.postResponseRejected(error, callback);
        expect(executionHandler.handleError.callCount).to.equal(1);
        expect(executionHandler.handleError.getCall(0).args[0]).to.equal(error);
        expect(executionHandler.handleError.getCall(0).args[1]).to.equal(callback);
      });
    });

    describe('handle', () => {
      // handle the middleware that executes in one of:
      // [onRequest, onResponse, onHttpResponseReturned, onNonHttpResponseReturned,
      // onHttpResponseThrown, onNonHttpResponseThrown]

      describe('when called with valid config', () => {
        const sandbox = sinon.createSandbox();
        const config = {
          onRequest: sandbox.fake(),
          onResponse: sandbox.fake(),
          onHttpResponseReturned: sandbox.fake(),
          onNonHttpResponseReturned: sandbox.fake(),
          onHttpResponseThrown: sandbox.fake(),
          onNonHttpResponseThrown: sandbox.fake(),
          handleError: sandbox.fake(),
          handleResponse: sandbox.fake(),
          handleNoResponse: sandbox.fake()
        }

        let payload = {
          event: {},
          context: {},
          callback: sandbox.fake()
        };

        let executionHandler;

        beforeEach(() => {
          executionHandler = new ExecutionHandler(config);
        });

        afterEach(() => {
          sandbox.resetHistory();
        });

        it('should call processChainOfFunctions with onRequest method and supplied payload', async () => {
          sandbox.stub(ExecutionHandler, 'processChainOfFunctions').resolves();
          await expect(
            executionHandler.handle(payload.event, payload.context, payload.callback)
          ).to.eventually.be.fulfilled;
          // supplied onRequest function is converted to an array containing that function
          expect(ExecutionHandler.processChainOfFunctions.getCall(0).args[0][0]).to.equal(config.onRequest);
          expect(ExecutionHandler.processChainOfFunctions.getCall(0).args[1].event).to.equal(payload.event);
          expect(ExecutionHandler.processChainOfFunctions.getCall(0).args[1].context).to.equal(payload.context);
          ExecutionHandler.processChainOfFunctions.restore();
        });

        it('should call processChainOfFunctions providing an empty object as shared parameter', async () => {
          sandbox.stub(ExecutionHandler, 'processChainOfFunctions').resolves();
          await expect(
            executionHandler.handle(payload.event, payload.context, payload.callback)
          ).to.eventually.be.fulfilled;
          expect(ExecutionHandler.processChainOfFunctions.getCall(0).args[1].shared).to.deep.equal({});
          ExecutionHandler.processChainOfFunctions.restore();
        });

        describe('when call to top-level processChainOfFunctions resolves the promise', () => {
          describe('without a value', () => {
            before(() => {
              sandbox.stub(ExecutionHandler, 'processChainOfFunctions').resolves();
            })

            after(() => {
              ExecutionHandler.processChainOfFunctions.restore();
            });

            it('should call handleNoResponse with the provided callback', async () => {
              await expect(
                executionHandler.handle(payload.event, payload.context, payload.callback)
              ).to.eventually.be.fulfilled;
              expect(config.handleNoResponse.called).to.be.true;
              expect(config.handleNoResponse.getCall(0).args[0]).to.equal(payload.callback);
            });
          });

          describe('with an HttpResponse', () => {
            const onRequestResponse = new HttpResponse();

            it('should call processChainOfFunctions with onHttpResponseReturned, supplied payload, shared object and response from onRequest chain functions', async () => {
              sandbox.stub(ExecutionHandler, 'processChainOfFunctions').resolves(onRequestResponse);

              await expect(
                executionHandler.handle(payload.event, payload.context, payload.callback)
              ).to.eventually.be.fulfilled;
              // supplied onHttpResponseReturned function is converted to an array containing that function
              expect(ExecutionHandler.processChainOfFunctions.getCall(1).args[0][0]).to.equal(config.onHttpResponseReturned);
              expect(ExecutionHandler.processChainOfFunctions.getCall(1).args[1].event).to.equal(payload.event);
              expect(ExecutionHandler.processChainOfFunctions.getCall(1).args[1].context).to.equal(payload.context);
              expect(ExecutionHandler.processChainOfFunctions.getCall(1).args[1].shared).to.equal(ExecutionHandler.processChainOfFunctions.getCall(0).args[1].shared);
              expect(ExecutionHandler.processChainOfFunctions.getCall(1).args[1].response).to.equal(onRequestResponse);

              ExecutionHandler.processChainOfFunctions.restore();
            });

            describe('when call to inner processChainOfFunctions resolves the promise', () => {
              const onHttpResponseReturnedResponse = {};

              it('should call processChainOfFunctions with onResponse, supplied payload, shared object and response from onNonHttpResponseReturned', async () => {
                sandbox.stub(ExecutionHandler, 'processChainOfFunctions')
                  .onCall(0).resolves(onRequestResponse)
                  .onCall(1).resolves(onHttpResponseReturnedResponse);

                await expect(
                  executionHandler.handle(payload.event, payload.context, payload.callback)
                ).to.eventually.be.fulfilled;
                // supplied onHttpResponseReturned function is converted to an array containing that function
                expect(ExecutionHandler.processChainOfFunctions.getCall(2).args[0][0]).to.equal(config.onResponse);
                expect(ExecutionHandler.processChainOfFunctions.getCall(2).args[1].event).to.equal(payload.event);
                expect(ExecutionHandler.processChainOfFunctions.getCall(2).args[1].context).to.equal(payload.context);
                expect(ExecutionHandler.processChainOfFunctions.getCall(2).args[1].shared).to.equal(ExecutionHandler.processChainOfFunctions.getCall(1).args[1].shared);
                expect(ExecutionHandler.processChainOfFunctions.getCall(2).args[1].response).to.equal(onHttpResponseReturnedResponse);

                ExecutionHandler.processChainOfFunctions.restore();
              });

              describe('when call to bottom-level processChainOfFunctions resolves the promise', () => {
                const onResponseResponse = {};

                it('should call postResponseResolved with the response and callback', async () => {
                  sandbox.stub(ExecutionHandler, 'processChainOfFunctions')
                    .onCall(0).resolves(onRequestResponse)
                    .onCall(1).resolves(onHttpResponseReturnedResponse)
                    .onCall(2).resolves(onResponseResponse);

                  sandbox.stub(executionHandler, 'postResponseResolved');

                  await expect(
                    executionHandler.handle(payload.event, payload.context, payload.callback)
                  ).to.eventually.be.fulfilled;
                  // supplied onHttpResponseReturned function is converted to an array containing that function
                  expect(executionHandler.postResponseResolved.getCall(0).args[0]).to.equal(onResponseResponse);
                  expect(executionHandler.postResponseResolved.getCall(0).args[1]).to.equal(payload.callback);

                  ExecutionHandler.processChainOfFunctions.restore();
                });
              });

              describe('when call to bottom-level processChainOfFunctions rejects the promise', () => {
                const onHttpResponseReturnedResponse = {};
                const onResponseError = {};

                it('should call postResponseRejected with the error and callback', async () => {
                  sandbox.stub(ExecutionHandler, 'processChainOfFunctions')
                    .onCall(0).resolves(onRequestResponse)
                    .onCall(1).resolves(onHttpResponseReturnedResponse)
                    .onCall(2).rejects(onResponseError);
                  sandbox.stub(executionHandler, 'postResponseRejected');

                  await expect(
                    executionHandler.handle(payload.event, payload.context, payload.callback)
                  ).to.eventually.be.fulfilled;
                  // supplied onHttpResponseReturned function is converted to an array containing that function
                  expect(executionHandler.postResponseRejected.getCall(0).args[0]).to.equal(onResponseError);
                  expect(executionHandler.postResponseRejected.getCall(0).args[1]).to.equal(payload.callback);

                  ExecutionHandler.processChainOfFunctions.restore();
                });
              });
            });

            describe('when call to inner processChainOfFunctions rejects the promise', () => {
              const onResponseError = {};

              it('should call postResponseRejected with the error and callback', async () => {
                sandbox.stub(ExecutionHandler, 'processChainOfFunctions')
                  .onCall(0).resolves(onRequestResponse)
                  .onCall(1).rejects(onResponseError);
                sandbox.stub(executionHandler, 'postResponseRejected');

                await expect(
                  executionHandler.handle(payload.event, payload.context, payload.callback)
                ).to.eventually.be.fulfilled;
                // supplied onHttpResponseReturned function is converted to an array containing that function
                expect(executionHandler.postResponseRejected.getCall(0).args[0]).to.equal(onResponseError);
                expect(executionHandler.postResponseRejected.getCall(0).args[1]).to.equal(payload.callback);

                ExecutionHandler.processChainOfFunctions.restore();
              });
            });
          });

          describe('with a non HttpResponse', () => {
            const onRequestResponse = {};

            it('should call processChainOfFunctions with onHttpResponseReturned, supplied payload, shared object and response from onRequest chain functions', async () => {
              sandbox.stub(ExecutionHandler, 'processChainOfFunctions').resolves(onRequestResponse);

              await expect(
                executionHandler.handle(payload.event, payload.context, payload.callback)
              ).to.eventually.be.fulfilled;
              // supplied onNonHttpResponseReturned function is converted to an array containing that function
              expect(ExecutionHandler.processChainOfFunctions.getCall(1).args[0][0]).to.equal(config.onNonHttpResponseReturned);
              expect(ExecutionHandler.processChainOfFunctions.getCall(1).args[1].event).to.equal(payload.event);
              expect(ExecutionHandler.processChainOfFunctions.getCall(1).args[1].context).to.equal(payload.context);
              expect(ExecutionHandler.processChainOfFunctions.getCall(1).args[1].shared).to.equal(ExecutionHandler.processChainOfFunctions.getCall(0).args[1].shared);
              expect(ExecutionHandler.processChainOfFunctions.getCall(1).args[1].response).to.equal(onRequestResponse);

              ExecutionHandler.processChainOfFunctions.restore();
            });

            describe('when call to inner processChainOfFunctions resolves the promise', () => {
              const onNonHttpResponseReturnedResponse = {};

              it('should call processChainOfFunctions with onResponse, supplied payload, shared object and response from onNonHttpResponseReturned', async () => {
                sandbox.stub(ExecutionHandler, 'processChainOfFunctions')
                  .onCall(0).resolves(onRequestResponse)
                  .onCall(1).resolves(onNonHttpResponseReturnedResponse);

                await expect(
                  executionHandler.handle(payload.event, payload.context, payload.callback)
                ).to.eventually.be.fulfilled;
                // supplied onHttpResponseReturned function is converted to an array containing that function
                expect(ExecutionHandler.processChainOfFunctions.getCall(2).args[0][0]).to.equal(config.onResponse);
                expect(ExecutionHandler.processChainOfFunctions.getCall(2).args[1].event).to.equal(payload.event);
                expect(ExecutionHandler.processChainOfFunctions.getCall(2).args[1].context).to.equal(payload.context);
                expect(ExecutionHandler.processChainOfFunctions.getCall(2).args[1].shared).to.equal(ExecutionHandler.processChainOfFunctions.getCall(1).args[1].shared);
                expect(ExecutionHandler.processChainOfFunctions.getCall(2).args[1].response).to.equal(onNonHttpResponseReturnedResponse);

                ExecutionHandler.processChainOfFunctions.restore();
              });

              describe('when call to bottom-level processChainOfFunctions resolves the promise', () => {
                const onResponseResponse = {};

                it('should call postResponseResolved with the response and callback', async () => {
                  sandbox.stub(ExecutionHandler, 'processChainOfFunctions')
                    .onCall(0).resolves(onRequestResponse)
                    .onCall(1).resolves(onNonHttpResponseReturnedResponse)
                    .onCall(2).resolves(onResponseResponse);

                  sandbox.stub(executionHandler, 'postResponseResolved');

                  await expect(
                    executionHandler.handle(payload.event, payload.context, payload.callback)
                  ).to.eventually.be.fulfilled;
                  // supplied onHttpResponseReturned function is converted to an array containing that function
                  expect(executionHandler.postResponseResolved.getCall(0).args[0]).to.equal(onResponseResponse);
                  expect(executionHandler.postResponseResolved.getCall(0).args[1]).to.equal(payload.callback);

                  ExecutionHandler.processChainOfFunctions.restore();
                });
              });

              describe('when call to bottom-level processChainOfFunctions rejects the promise', () => {
                const onHttpResponseReturnedResponse = {};
                const onResponseError = {};

                it('should call postResponseRejected with the error and callback', async () => {
                  sandbox.stub(ExecutionHandler, 'processChainOfFunctions')
                    .onCall(0).resolves(onRequestResponse)
                    .onCall(1).resolves(onHttpResponseReturnedResponse)
                    .onCall(2).rejects(onResponseError);
                  sandbox.stub(executionHandler, 'postResponseRejected');

                  await expect(
                    executionHandler.handle(payload.event, payload.context, payload.callback)
                  ).to.eventually.be.fulfilled;
                  // supplied onHttpResponseReturned function is converted to an array containing that function
                  expect(executionHandler.postResponseRejected.getCall(0).args[0]).to.equal(onResponseError);
                  expect(executionHandler.postResponseRejected.getCall(0).args[1]).to.equal(payload.callback);

                  ExecutionHandler.processChainOfFunctions.restore();
                });
              });
            });

            describe('when call to inner processChainOfFunctions rejects the promise', () => {
              const onResponseError = {};

              it('should call postResponseRejected with the error and callback', async () => {
                sandbox.stub(ExecutionHandler, 'processChainOfFunctions')
                  .onCall(0).resolves(onRequestResponse)
                  .onCall(1).rejects(onResponseError);
                sandbox.stub(executionHandler, 'postResponseRejected');

                await expect(
                  executionHandler.handle(payload.event, payload.context, payload.callback)
                ).to.eventually.be.fulfilled;
                // supplied onHttpResponseReturned function is converted to an array containing that function
                expect(executionHandler.postResponseRejected.getCall(0).args[0]).to.equal(onResponseError);
                expect(executionHandler.postResponseRejected.getCall(0).args[1]).to.equal(payload.callback);

                ExecutionHandler.processChainOfFunctions.restore();
              });
            });
          });
        });

        describe('when call to top-level processChainOfFunctions rejects the promise', () => {
          describe('when error is an instance of HttpResponse', () => {
            const onRequestError = new HttpResponse();

            it('should call processChainOfFunctions with onHttpResponseThrown, supplied payload, shared object and response from onRequest chain functions', async () => {
              sandbox.stub(ExecutionHandler, 'processChainOfFunctions').rejects(onRequestError);

              await expect(
                executionHandler.handle(payload.event, payload.context, payload.callback)
              ).to.eventually.be.fulfilled;
              // supplied onNonHttpResponseReturned function is converted to an array containing that function
              expect(ExecutionHandler.processChainOfFunctions.getCall(1).args[0][0]).to.equal(config.onHttpResponseThrown);
              expect(ExecutionHandler.processChainOfFunctions.getCall(1).args[1].event).to.equal(payload.event);
              expect(ExecutionHandler.processChainOfFunctions.getCall(1).args[1].context).to.equal(payload.context);
              expect(ExecutionHandler.processChainOfFunctions.getCall(1).args[1].shared).to.equal(ExecutionHandler.processChainOfFunctions.getCall(0).args[1].shared);
              expect(ExecutionHandler.processChainOfFunctions.getCall(1).args[1].error).to.equal(onRequestError);

              ExecutionHandler.processChainOfFunctions.restore();
            });

            describe('when call to inner processChainOfFunctions resolves the promise', () => {
              const onHttpResponseThrownResponse = {};

              it('should call processChainOfFunctions with onResponse, supplied payload, shared object and response from onNonHttpResponseReturned', async () => {
                sandbox.stub(ExecutionHandler, 'processChainOfFunctions')
                  .onCall(0).rejects(onRequestError)
                  .onCall(1).resolves(onHttpResponseThrownResponse);

                await expect(
                  executionHandler.handle(payload.event, payload.context, payload.callback)
                ).to.eventually.be.fulfilled;
                // supplied onHttpResponseReturned function is converted to an array containing that function
                expect(ExecutionHandler.processChainOfFunctions.getCall(2).args[0][0]).to.equal(config.onResponse);
                expect(ExecutionHandler.processChainOfFunctions.getCall(2).args[1].event).to.equal(payload.event);
                expect(ExecutionHandler.processChainOfFunctions.getCall(2).args[1].context).to.equal(payload.context);
                expect(ExecutionHandler.processChainOfFunctions.getCall(2).args[1].shared).to.equal(ExecutionHandler.processChainOfFunctions.getCall(1).args[1].shared);
                expect(ExecutionHandler.processChainOfFunctions.getCall(2).args[1].response).to.equal(onHttpResponseThrownResponse);

                ExecutionHandler.processChainOfFunctions.restore();
              });

              describe('when call to bottom-level processChainOfFunctions resolves the promise', () => {
                const onResponseResponse = {};

                it('should call postResponseResolved with the response and callback', async () => {
                  sandbox.stub(ExecutionHandler, 'processChainOfFunctions')
                    .onCall(0).rejects(onRequestError)
                    .onCall(1).resolves(onHttpResponseThrownResponse)
                    .onCall(2).resolves(onResponseResponse);

                  sandbox.stub(executionHandler, 'postResponseResolved');

                  await expect(
                    executionHandler.handle(payload.event, payload.context, payload.callback)
                  ).to.eventually.be.fulfilled;
                  // supplied onHttpResponseReturned function is converted to an array containing that function
                  expect(executionHandler.postResponseResolved.getCall(0).args[0]).to.equal(onResponseResponse);
                  expect(executionHandler.postResponseResolved.getCall(0).args[1]).to.equal(payload.callback);

                  ExecutionHandler.processChainOfFunctions.restore();
                });
              });

              describe('when call to bottom-level processChainOfFunctions rejects the promise', () => {
                const onHttpResponseReturnedResponse = {};
                const onResponseError = {};

                it('should call postResponseRejected with the error and callback', async () => {
                  sandbox.stub(ExecutionHandler, 'processChainOfFunctions')
                    .onCall(0).resolves(onRequestError)
                    .onCall(1).resolves(onHttpResponseReturnedResponse)
                    .onCall(2).rejects(onResponseError);
                  sandbox.stub(executionHandler, 'postResponseRejected');

                  await expect(
                    executionHandler.handle(payload.event, payload.context, payload.callback)
                  ).to.eventually.be.fulfilled;
                  // supplied onHttpResponseReturned function is converted to an array containing that function
                  expect(executionHandler.postResponseRejected.getCall(0).args[0]).to.equal(onResponseError);
                  expect(executionHandler.postResponseRejected.getCall(0).args[1]).to.equal(payload.callback);

                  ExecutionHandler.processChainOfFunctions.restore();
                });
              });
            });

            describe('when call to inner processChainOfFunctions rejects the promise', () => {
              const onResponseError = {};

              it('should call postResponseRejected with the error and callback', async () => {
                sandbox.stub(ExecutionHandler, 'processChainOfFunctions')
                  .onCall(0).resolves(onRequestError)
                  .onCall(1).rejects(onResponseError);
                sandbox.stub(executionHandler, 'postResponseRejected');

                await expect(
                  executionHandler.handle(payload.event, payload.context, payload.callback)
                ).to.eventually.be.fulfilled;
                // supplied onHttpResponseReturned function is converted to an array containing that function
                expect(executionHandler.postResponseRejected.getCall(0).args[0]).to.equal(onResponseError);
                expect(executionHandler.postResponseRejected.getCall(0).args[1]).to.equal(payload.callback);

                ExecutionHandler.processChainOfFunctions.restore();
              });
            });
          });

          describe('when error is not an instance of HttpResponse', () => {
            const onRequestError = {};

            it('should call processChainOfFunctions with onHttpResponseThrown, supplied payload, shared object and response from onRequest chain functions', async () => {
              sandbox.stub(ExecutionHandler, 'processChainOfFunctions').rejects(onRequestError);

              await expect(
                executionHandler.handle(payload.event, payload.context, payload.callback)
              ).to.eventually.be.fulfilled;
              // supplied onNonHttpResponseReturned function is converted to an array containing that function
              expect(ExecutionHandler.processChainOfFunctions.getCall(1).args[0][0]).to.equal(config.onNonHttpResponseThrown);
              expect(ExecutionHandler.processChainOfFunctions.getCall(1).args[1].event).to.equal(payload.event);
              expect(ExecutionHandler.processChainOfFunctions.getCall(1).args[1].context).to.equal(payload.context);
              expect(ExecutionHandler.processChainOfFunctions.getCall(1).args[1].shared).to.equal(ExecutionHandler.processChainOfFunctions.getCall(0).args[1].shared);
              expect(ExecutionHandler.processChainOfFunctions.getCall(1).args[1].error).to.equal(onRequestError);

              ExecutionHandler.processChainOfFunctions.restore();
            });

            describe('when call to inner processChainOfFunctions resolves the promise', () => {
              const onNonHttpResponseThrownResponse = {};

              it('should call processChainOfFunctions with onResponse, supplied payload, shared object and response from onNonHttpResponseReturned', async () => {
                sandbox.stub(ExecutionHandler, 'processChainOfFunctions')
                  .onCall(0).rejects(onRequestError)
                  .onCall(1).resolves(onNonHttpResponseThrownResponse);

                await expect(
                  executionHandler.handle(payload.event, payload.context, payload.callback)
                ).to.eventually.be.fulfilled;
                // supplied onHttpResponseReturned function is converted to an array containing that function
                expect(ExecutionHandler.processChainOfFunctions.getCall(2).args[0][0]).to.equal(config.onResponse);
                expect(ExecutionHandler.processChainOfFunctions.getCall(2).args[1].event).to.equal(payload.event);
                expect(ExecutionHandler.processChainOfFunctions.getCall(2).args[1].context).to.equal(payload.context);
                expect(ExecutionHandler.processChainOfFunctions.getCall(2).args[1].shared).to.equal(ExecutionHandler.processChainOfFunctions.getCall(1).args[1].shared);
                expect(ExecutionHandler.processChainOfFunctions.getCall(2).args[1].response).to.equal(onNonHttpResponseThrownResponse);

                ExecutionHandler.processChainOfFunctions.restore();
              });

              describe('when call to bottom-level processChainOfFunctions resolves the promise', () => {
                const onResponseResponse = {};

                it('should call postResponseResolved with the response and callback', async () => {
                  sandbox.stub(ExecutionHandler, 'processChainOfFunctions')
                    .onCall(0).rejects(onRequestError)
                    .onCall(1).resolves(onNonHttpResponseThrownResponse)
                    .onCall(2).resolves(onResponseResponse);

                  sandbox.stub(executionHandler, 'postResponseResolved');

                  await expect(
                    executionHandler.handle(payload.event, payload.context, payload.callback)
                  ).to.eventually.be.fulfilled;
                  // supplied onHttpResponseReturned function is converted to an array containing that function
                  expect(executionHandler.postResponseResolved.getCall(0).args[0]).to.equal(onResponseResponse);
                  expect(executionHandler.postResponseResolved.getCall(0).args[1]).to.equal(payload.callback);

                  ExecutionHandler.processChainOfFunctions.restore();
                });
              });

              describe('when call to bottom-level processChainOfFunctions rejects the promise', () => {
                const onHttpResponseReturnedResponse = {};
                const onResponseError = {};

                it('should call postResponseRejected with the error and callback', async () => {
                  sandbox.stub(ExecutionHandler, 'processChainOfFunctions')
                    .onCall(0).resolves(onRequestError)
                    .onCall(1).resolves(onHttpResponseReturnedResponse)
                    .onCall(2).rejects(onResponseError);
                  sandbox.stub(executionHandler, 'postResponseRejected');

                  await expect(
                    executionHandler.handle(payload.event, payload.context, payload.callback)
                  ).to.eventually.be.fulfilled;
                  // supplied onHttpResponseReturned function is converted to an array containing that function
                  expect(executionHandler.postResponseRejected.getCall(0).args[0]).to.equal(onResponseError);
                  expect(executionHandler.postResponseRejected.getCall(0).args[1]).to.equal(payload.callback);

                  ExecutionHandler.processChainOfFunctions.restore();
                });
              });
            });

            describe('when call to inner processChainOfFunctions rejects the promise', () => {
              const onResponseError = {};

              it('should call postResponseRejected with the error and callback', async () => {
                sandbox.stub(ExecutionHandler, 'processChainOfFunctions')
                  .onCall(0).resolves(onRequestError)
                  .onCall(1).rejects(onResponseError);
                sandbox.stub(executionHandler, 'postResponseRejected');

                await expect(
                  executionHandler.handle(payload.event, payload.context, payload.callback)
                ).to.eventually.be.fulfilled;
                // supplied onHttpResponseReturned function is converted to an array containing that function
                expect(executionHandler.postResponseRejected.getCall(0).args[0]).to.equal(onResponseError);
                expect(executionHandler.postResponseRejected.getCall(0).args[1]).to.equal(payload.callback);

                ExecutionHandler.processChainOfFunctions.restore();
              });
            });
          });
        });
      });

      // TODO: Shared object should persist between calls to processChainOfFunctions
    });
  });
});

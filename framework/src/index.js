const ExecutionHandler = require('./ExecutionHandler');
const HttpResponse = require('./HttpResponse');

function logRequest({event, context, shared}) {
  console.log({event});
}

function handler({event, shared}) {
  shared.value = ['onRequest (handle)'];
  switch(event.value) {
    case 0:
      return;
    case 1:
      return 1;
    case 2:
      return new HttpResponse();
    case 3:
      throw {};
    case 4:
      throw new Error('Test error');
    case 5:
      throw new HttpResponse();
  }
}


function handleError(error, callback) {
  callback(null, {
    finalHandler: 'handleError',
    error,
  });
}

function handleResponse(response, callback) {
  callback(null, {
    finalHandler: 'handleResponse',
    response
  });
}

function handleNoResponse(callback) {
  callback(null, {
    finalHandler: 'handleNoResponse'
  });
}

function onHttpResponseReturned(httpResponse) {
  httpResponse.shared.value.push('onHttpResponseReturned');
  console.log({shared: httpResponse.shared});
  return 'onHttpResponseReturned';
}

function onNonHttpResponseReturned(response) {
  response.shared.value.push('onNonHttpResponseReturned');
  console.log({shared: response.shared});
  return 'onNonHttpResponseReturned';
}

function onHttpResponseThrown(httpResponse) {
  httpResponse.shared.value.push('onHttpResponseThrown');
  console.log({shared: httpResponse.shared});
  return 'onHttpResponseThrown';
}

function onNonHttpResponseThrown(response) {
  response.shared.value.push('onNonHttpResponseThrown');
  console.log({shared: response.shared});
  return 'onNonHttpResponseThrown';
}


async function main() {
  function callback(error, response) {
    console.log('Callback called with:', {error, response});
  }

  const eh = new ExecutionHandler({
    onRequest: [
      logRequest,
      handler
    ],
    onHttpResponseReturned,
    onNonHttpResponseReturned,
    onHttpResponseThrown,
    onNonHttpResponseThrown,
    handleError,
    handleResponse,
    handleNoResponse,
  });

  const event = {
    value: 2
  }


  await eh.handle(event, {}, callback);
}

main();

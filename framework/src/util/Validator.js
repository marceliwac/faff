const assert = require('assert');

class Validator {
  static isFunction(x) {
    return typeof x === 'function';
  }

  static isArrayOfFunctions(x) {
    return Array.isArray(x) && x.every(e => Validator.isFunction(e))
  }

  static isNonEmptyArrayOfFunctions(x) {
    return Validator.isArrayOfFunctions(x) && x.length > 0;
  }

  static isFunctionOrArrayOfFunctions(x) {
    return Validator.isFunction(x) || Validator.isArrayOfFunctions(x);
  }

  static isFunctionOrNonEmptyArrayOfFunctions(x) {
    return Validator.isFunction(x) || Validator.isNonEmptyArrayOfFunctions(x);
  }

  static validateConfig(config, allFieldsRequired = true) {
    if (allFieldsRequired || Object.prototype.hasOwnProperty.call(config, 'onRequest')) {
      assert(
        Validator.isFunctionOrArrayOfFunctions(config.onRequest),
        new TypeError('Invalid config - config.onRequest has to be a function, empty array or an array of functions.')
      );
    }
    if (allFieldsRequired || Object.prototype.hasOwnProperty.call(config, 'onResponse')) {
      assert(
        Validator.isFunctionOrArrayOfFunctions(config.onResponse),
        new TypeError('Invalid config - config.onResponse has to be a function, empty array or an array of functions.')
      );
    }
    if (allFieldsRequired || Object.prototype.hasOwnProperty.call(config, 'onHttpResponseReturned')) {
      assert(
        Validator.isFunctionOrArrayOfFunctions(config.onHttpResponseReturned),
        new TypeError('Invalid config - config.onHttpResponseReturned has to be a function, empty array or an array of functions.')
      );
    }
    if (allFieldsRequired || Object.prototype.hasOwnProperty.call(config, 'onNonHttpResponseReturned')) {
      assert(
        Validator.isFunctionOrArrayOfFunctions(config.onNonHttpResponseReturned),
        new TypeError('Invalid config - config.onNonHttpResponseReturned has to be a function, empty array or an array of functions.')
      );
    }
    if (allFieldsRequired || Object.prototype.hasOwnProperty.call(config, 'onHttpResponseThrown')) {
      assert(
        Validator.isFunctionOrArrayOfFunctions(config.onHttpResponseThrown),
        new TypeError('Invalid config - config.onHttpResponseThrown has to be a function, empty array or an array of functions.')
      );
    }
    if (allFieldsRequired || Object.prototype.hasOwnProperty.call(config, 'onNonHttpResponseThrown')) {
      assert(
        Validator.isFunctionOrArrayOfFunctions(config.onNonHttpResponseThrown),
        new TypeError('Invalid config - config.onNonHttpResponseThrown has to be a function, empty array or an array of functions.')
      );
    }
    if (allFieldsRequired || Object.prototype.hasOwnProperty.call(config, 'handleError')) {
      assert(
        Validator.isFunction(config.handleError),
        new TypeError('Invalid config - config.handleError has to be a function.')
      );
    }
    if (allFieldsRequired || Object.prototype.hasOwnProperty.call(config, 'handleResponse')) {
      assert(
        Validator.isFunction(config.handleResponse),
        new TypeError('Invalid config - config.handleResponse has to be a function.')
      );
    }
    if (allFieldsRequired || Object.prototype.hasOwnProperty.call(config, 'handleNoResponse')) {
      assert(
        Validator.isFunction(config.handleNoResponse),
        new TypeError('Invalid config - config.handleNoResponse has to be a function.')
      );
    }
  }
}

module.exports = Validator;

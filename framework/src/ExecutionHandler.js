const Validator = require("./util/Validator");
const HttpResponse = require('./HttpResponse');

class ExecutionHandler {
  static DEFAULT_CONFIG = {
    onRequest: () => {},
    onResponse: () => {},
    onHttpResponseReturned: () => {},
    onNonHttpResponseReturned: () => {},
    onHttpResponseThrown: () => {},
    onNonHttpResponseThrown: () => {},
    handleError: () => {},
    handleResponse: () => {},
    handleNoResponse: () => {}
  };

  /**
   * Assemble config using supplied values where provided or use defaults where the specific config
   * values are missing.
   */
  static assembleConfig(config) {
    if(typeof config === 'undefined') {
      return ExecutionHandler.DEFAULT_CONFIG;
    }
    if(typeof config !== 'object') {
      throw new TypeError('Provided configuration has to be an object or be undefined!');
    }
    let assembledConfig = ExecutionHandler.DEFAULT_CONFIG;
    for (let key in ExecutionHandler.DEFAULT_CONFIG) {
      if (Object.prototype.hasOwnProperty.call(config, key)) {
        assembledConfig[key] = config[key];
      }
    }
    return assembledConfig;
  }

  /**
   * Execute supplied functions in order until a function returns or throws an error. Each function
   * is supplied provided payload as an argument.
   *
   * If payload has a response parameter defined, that
   * parameter will be passed down to the next function (to allow for handling of the responses from
   * onHttpResponseReturned etc. when called with onResponse).
   *
   * If an error is thrown, this function does not catch it.
   */
  static async processChainOfFunctions(arrayOfFunctions, payload) {
    if (!Validator.isArrayOfFunctions(arrayOfFunctions)) {
      throw new TypeError('Supplied argument has to be an array of functions or an empty array.');
    }
    let result;
    for (let i = 0; i < arrayOfFunctions.length; i++) {
      result = await arrayOfFunctions[i](payload);
      if (result !== undefined) {
        return result;
      }
    }
    if (result === undefined && payload.response !== undefined) {
      return payload.response;
    }
  }

  constructor(config) {
    if (typeof config === 'object') {
        Validator.validateConfig(config, false);
      }
    const assembledConfig = ExecutionHandler.assembleConfig(config);
    Validator.validateConfig(assembledConfig);
    this.initialise(assembledConfig);
  }

  /**
   * Initialise the config with supplied  components (function or array of functions) to a flattened
   * array of functions.
   */
  initialise(config) {
    //
    this.onRequest = [config.onRequest].flat();
    this.onResponse = [config.onResponse].flat();
    this.onHttpResponseReturned = [config.onHttpResponseReturned].flat();
    this.onNonHttpResponseReturned = [config.onNonHttpResponseReturned].flat();
    this.onHttpResponseThrown = [config.onHttpResponseThrown].flat();
    this.onNonHttpResponseThrown = [config.onNonHttpResponseThrown].flat();
    this.handleError = config.handleError;
    this.handleResponse = config.handleResponse;
    this.handleNoResponse = config.handleNoResponse;
  }

  postResponseResolved(handlerResponse, callback) {
    if(handlerResponse !== undefined) {
      return this.handleResponse(handlerResponse, callback);
    }
    this.handleNoResponse(callback);
  }

  postResponseRejected(handlerError, callback) {
    this.handleError(handlerError, callback);
  }

  handle(event, context, callback) {
    const shared = {};
    return ExecutionHandler.processChainOfFunctions(this.onRequest, {event, context, shared})
      .then(async (onRequestResponse) => {
        if(onRequestResponse === undefined) {
          return this.handleNoResponse(callback);
        } else {
          if (onRequestResponse instanceof HttpResponse) {
            return ExecutionHandler.processChainOfFunctions(this.onHttpResponseReturned, {event, context, shared, response: onRequestResponse})
              .then((onHttpResponseReturnedResponse) => ExecutionHandler.processChainOfFunctions(this.onResponse, {event, context, shared, response: onHttpResponseReturnedResponse}))
              .then((onResponseResponse) => this.postResponseResolved(onResponseResponse, callback))
              .catch((onResponseError) => this.postResponseRejected(onResponseError, callback));
          } else {
            return ExecutionHandler.processChainOfFunctions(this.onNonHttpResponseReturned, {event, context, shared, response: onRequestResponse})
              .then((onNonHttpResponseReturnedResponse) => ExecutionHandler.processChainOfFunctions(this.onResponse, {event, context, shared, response: onNonHttpResponseReturnedResponse}))
              .then((onResponseResponse) => this.postResponseResolved(onResponseResponse, callback))
              .catch((onResponseError) => this.postResponseRejected(onResponseError, callback));
          }
        }
      }).catch((onRequestError) => {
        if (onRequestError instanceof HttpResponse) {
          return ExecutionHandler.processChainOfFunctions(this.onHttpResponseThrown, {event, context, shared, error: onRequestError})
            .then((onHttpResponseThrownResponse) => ExecutionHandler.processChainOfFunctions(this.onResponse, {event, context, shared, response: onHttpResponseThrownResponse}))
            .then((onResponseResponse) => this.postResponseResolved(onResponseResponse, callback))
            .catch((onResponseError) => this.postResponseRejected(onResponseError, callback));
        } else {
          return ExecutionHandler.processChainOfFunctions(this.onNonHttpResponseThrown, {event, context, shared, error: onRequestError})
            .then((onNonHttpResponseThrownResponse) => ExecutionHandler.processChainOfFunctions(this.onResponse, {event, context, shared, response: onNonHttpResponseThrownResponse}))
            .then((onResponseResponse) => this.postResponseResolved(onResponseResponse, callback))
            .catch((onResponseError) => this.postResponseRejected(onResponseError, callback));
        }
    });
  }

}

module.exports = ExecutionHandler;

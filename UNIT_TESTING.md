# Unit testing overview
## Tools 

Unit tests for this framework are implemented using [Mocha](https://github.com/mochajs/mocha) (for 
overall testing framework), [Sinon](https://github.com/sinonjs/sinon) (for implementing spies, 
mocks and stubs) and [Chai](https://github.com/chaijs/chai) (for writing assertions). 

Additional utilities for these tools such as libraries extending the assertion capabilities like
[Chai-as-promised](https://github.com/domenic/chai-as-promised/) are also used.

## Framework

Conceptually, the testing framework used follows the 
[TDD (Test-driven development)](https://en.m.wikipedia.org/wiki/Test-driven_development) 
principles, in which:

1. Tests for the unit are developed first, outlining that unit's desired behaviour.
2. Tests initially fail.
3. Unit is then implemented incrementally to pass the tests. Code does not need to be elegant or 
optimal.
4. Implementation is verified with the tests to ensure that no regression has been introduced.
5. Implementation is refactored to adhere to coding style, remove unnecessarily repeated code, 
remove any potential overhead and generally optimise the implementation. Tests should be run in
alongside this process to ensure that the expected behaviour remains unchanged.
   
In other words, [red -> green -> 
refactor](https://www.codecademy.com/articles/tdd-red-green-refactor).

### Code style

The assertion style itself should follow the [BDD-style assertions](https://www.chaijs.com/api/bdd/)
(i.e.: `expect()`). Likewise, the language used for implementation of test suites should follow the 
[BDD-style DSL](https://mochajs.org/#bdd) with `describe()`, `context()` and `it()` directives.

The functions inside the test files should use lambda functions (`() => {}`) to implement the test 
suites. Variables shared across the multiple tests should be specified in the lowest scope possible 
allowing for re-usability of these variables (if they are not being modified during tests). 
For example, if a variety of tests inside a single test suite all use a common configuration object,
it should be defined at the root scope of that test suite. If a configuration object is used by 
multiple test suites, it should be defined within the same scope that holds these test suites.
For example:

```js
// Unit defined with describe()
describe('some unit', () => {
  
  // Value shared across multiple suites within the same suite ('MyClass')
  const commonConfig = {
    myKey: 'myVal'
  };

  // Inner test suites defined with context()
  context('when second paramter is provided', () => {
    context('when second parameter is a number', () => {

      // Single test defined with it()
      it('should throw an error', () => {

        // single assertion defined with expect()
        expect(() => someUnit(commonConfig, myFunction)).to.throw();
      });
    });

    // Inner test suites defined with context()
    context('when second parameter is a function', () => {
      
      // Value shared across multiple tests within the same suite ('when second parameter is a function')
      let myFunction;
      
      beforeEach(() => {
        myFunction = sinon.spy();
      });
      
      it('should not throw any errors', () => {
        expect(() => someUnit(commonConfig, myFunction)).to.not.throw();
      });

      it('should call the function once', () => {
        const myClass = someUnit(commonConfig, myFunction);

        // single assertion on object defined with expect()
        expect(myFunction.callCount).to.equal(1);
      });
    })
  });

  context('when second parameter is not provided', () => {
    it('should throw an error', () => {
      expect(() => someUnit(commonConfig)).to.throw();
    });
  });
})
```



# Methodology

## The process

The following methodology should serve as guidelines for developing the unit tests and implementing
software.

1. Depending on the type of unit (function or class (or object)), establish the test suite that will
contain the test suites for the system(s)-under-test. The name of the test suite should correspond
to the name of the tested unit. Test suites for classes outline additional structure that needs to
be followed (see below).
2. Analyse the dependencies of the unit. This should include any methods and properties of external
modules (i.e. modules imported from other files or libraries) as well as methods and properties
implemented by the encapsulating unit (i.e. class of the static method for which tests are being
written).
3. If the system-under-test is encapsulated by another unit (i.e. static properties, static methods,
constructor, instance methods):
   1. Extract the system-under-test from the encapsulated unit.
   2. Establish the stubbed environment that will encapsulate the unit to provide the necessary 
   interfaces for that unit to interact with.
4. Stub the dependencies of the unit, including external modules and functions.
5. Analyse the paths that can be taken by the unit during execution.
6. Extract the set of test suites that will need to be implemented to cover all possible execution
paths.
7. Scaffold the test suite by implementing necessary test suites depending on the possible execution
paths, and in the case of classes (or objects), the location of the unit within that class.
8. Implement the unit tests.


# Testing 
## Classes

In case of classes (or objects containing functions), each method should be treated as its own 
system-under-test and tested independently. These tests should be contained within the same test 
suite. Additionally, for classes a set structure should be followed where static properties, static
methods, constructor, and instance methods, all have their own separate test suites for clarity.
For example the following class should be tested with the test suite formatted as below:

```js
class MyClass {
  static myProperty = {};
  
  static doSomething() {
    // ...
  }
  
  constructor() {
    
  }
  
  doSomethingElse() {
    // ...
  }
}
```

```js
describe('MyClass', () => {
  describe('static properties', () => {
    describe('myProperty', () => {
      // Tests and test-suites should pertain to the MyClass.myProperty
    });
  });

  describe('static methods', () => {
    describe('doSomething', () => {
      // Tests and test-suites should pertain to the MyClass.doSomething
    });
  });

  describe('constructor', () => {
    // Tests and test-suites should pertain to the constructor of MyClass
  });
  
  describe('instance methods', () => {
    describe('doSomethingElse', () => {
      // Tests and test-suites should pertain to the MyClass.doSomethingElse
    });
  });
});
```
